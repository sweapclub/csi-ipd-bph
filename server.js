// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');

// Get our API routes
const apiQuestion = require('./server/routes/module/question');
const apiLanguage = require('./server/routes/module/language');
const apiCenterDB = require('./server/routes/module/centerDBLogin');
const apiSetting = require('./server/routes/module/setting');

const app = express();

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', "http://localhost:4200");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, X-XSRF-TOKEN');
  next();
})

//app.use(bodyParser());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// Set our api routes
app.use('/api/question', apiQuestion);
app.use('/api/language', apiLanguage);
app.use('/api/centerDB', apiCenterDB);
app.use('/api/setting', apiSetting);

app.use(function (err, req, res, next) {
  ``
  console.error(err.stack);
  res.status(500).send('Something broke!');
})

// Catch all other routes and return the index file
app.get('/InterpreterRequest*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, '0.0.0.0', () => console.log(`API running on localhost:${port}`));
