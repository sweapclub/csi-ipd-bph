export class Language {
    constructor(
        public nationID: string,
        public nationName: string,
        public nationShow: string
    ){}
}
