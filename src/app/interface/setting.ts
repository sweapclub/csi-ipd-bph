export class SettingLanguage {
    constructor(
        public nationID: string = "",
        public nationName: string = "",
        public nationShow: string = "",
        public nationActive: string = "0",
        public nationOldName: string = ""
    ) { }
}

export class SettingGroup {
    constructor(
        public groupID: string = "",
        public groupActive: string = "0",
        public groupOrder: number = null,
        public usePlace: string = "0",
        public groupName: string = ""
    ) { }
}

export class SettingGroupName {
    constructor(
        public nationName: string = "",
        public tranID: string = "",
        public nationID: string = "",
        public tranText: string = ""
    ) { }
}

export class SettingQuestion {
    constructor(
        public orderNo: number = 0,
        public questionID: string = "",
        public questionTypeID: string = "",
        public questionTypeName: string = "",
        public questionText: string = "",
        public questionActive: string = "1",
        public isForCalculation: string = ""
    ) { }
}

export class SettingQuestionModified {
    constructor(
        public questionID: string = "",
        public questionTypeID: string = "",
        public questionText: string = "",
        public nationID: string = "",
        public choice1: string = "",
        public score1: number = 0,
        public choice2: string = "",
        public score2: number = 0,
        public choice3: string = "",
        public score3: number = 0,
        public choice4: string = "",
        public score4: number = 0,
        public choice5: string = "",
        public score5: number = 0,
        public useTextboxFlg: string = "",
        public useTextboxAt: string = "",
        public questionActive: string = "",
        public nationName: string = ""

    ) { }
}

export class QuestionType {
    constructor(
        public typeID: string,
        public typeName: string,
        public typeDescription: string
    ) { }
}