export class TrakCarePatient {
    constructor (
        public patEN:string,
        public patHN:string,
        public patName:string,
        public patLastName:string,
        public parAge:string,
        public patSex:string,
        public patWard:string,
        public patRoom:string,
        public patDoctor:string,
        public patNation:string,
        public patAdm:Date,
        public patDOB:Date,
        public patDoctorCode: string,
        public patSocialStatus: string
    ) {}
}

export class PicturePatient {
    constructor(
        public LinkPhot:string
    ) {}
}