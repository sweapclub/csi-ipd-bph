export class QuestionInGroup {
    constructor(
        public domID: string,
        public groupID: string,
        public questionID: string,
        public groupText: String,
        public questionNo: String,
        public questionText: string,
        public nationID: string,
        public choice1: string,
        public score1: number,
        public choice2: string,
        public score2: number,
        public choice3: string,
        public score3: number,
        public choice4: string,
        public score4: number,
        public choice5: string,
        public score5: number,
        public useTextboxFlg: string,
        public useTextboxAt: number,
        public typeID: string,
        public typeName: string,
        public placeName: string,
        public placeID: string,
        public usePlace: number,

        public answer: number,
        public score: number,
        public placeSelected: boolean = false,
    ) {}
}

export class GroupQuestion {
    constructor (
        public groupID: string,
        public groupName: string
    ){}
}
