import { Component } from '@angular/core';

import { ApiService } from './service/api.service'
import { HelpService } from './service/help.service';
import { Subscription } from 'rxjs/Subscription'

import { QuestionInGroup } from './interface/questionaire';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ApiService]
})
export class AppComponent {
  title = 'app';

  subQry: Subscription;
  questions: QuestionInGroup;

  constructor(
    private api: ApiService,
    private helper: HelpService
  ) {

  }
}
