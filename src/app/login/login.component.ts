import { Component, OnInit } from '@angular/core';
import { HelpService } from '../service/help.service';

import { ApiService } from '../service/api.service';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userFlg = 'patient';
  HN = "";
  id = "";
  pwd = "";

  sub: any;
  constructor(
    private helper: HelpService,
    private api: ApiService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  loggin() {


    if (this.userFlg == 'patient') {

      this.sub = this.api.postTrakCarePatient(this.HN).subscribe(data => {
        if (data != null && (this.HN.length == 12)) {
          this.helper.tcPatient = data;
          this.helper.loginStatus = true;
          this.helper.userType = this.userFlg;
          this.helper.naviateTo('patient')
        } else {
          this.clearLogin();
          this.sbLogin('Please check HN is an ipd and correct format');          
        }
      })
    } else if (this.userFlg == 'employee') {
      if (this.id != "" && this.pwd != "") {
        this.sub = this.api.postEmployeeLogin(this.id, this.pwd).subscribe(data => {
          if (data.length != 0) {
            this.helper.employee = data;
            this.helper.loginStatus = true;
            this.helper.userType = this.userFlg;
            this.helper.naviateTo('employee');
          } else {
            this.clearLogin();
            this.sbLogin('Your Login is not correct');
          }
        })
      }
    }
  }

  private sbLogin(msg: string) {
    this.snackBar.open(msg, 'try again', {
      duration: 2000,
    });
  }

  private clearLogin() {
    this.HN = "";
    this.id = "";
    this.pwd = "";
  }


}
