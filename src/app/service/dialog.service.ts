import { Injectable } from '@angular/core';

import { MatDialog } from '@angular/material';
import { DialogLanguageComponent } from '../dialog/setting/dialog-language/dialog-language.component';
import { DialogGroupComponent } from '../dialog/setting/dialog-group/dialog-group.component';
import { DialogQuestionComponent } from '../dialog/setting/dialog-question/dialog-question.component';
@Injectable()
export class DialogService {

  constructor(
    private dialog: MatDialog
  ) { }

  openSettingLanguage(data = null){
    return this.dialog.open(DialogLanguageComponent, {
      data,
      width: '650px'
    })
  }

  openSettingGroup(data = null){
    return this.dialog.open(DialogGroupComponent, {
      data,
      width: '750px'
    })
  }

  openSettingQuestion(data = null){
    return this.dialog.open(DialogQuestionComponent, {
      data,
      width: '750px'
    })
  }
}
