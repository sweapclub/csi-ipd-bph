import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { TrakCarePatient } from '../interface/patient';
import { LoginData } from '../interface/employee';

@Injectable()
export class HelpService {

  loginStatus: boolean = false;
  userType: string = "";

  tcPatient: TrakCarePatient;
  employee: LoginData;

  constructor(
    private router: Router
  ) { }

  checkPermission() {
    if (this.loginStatus == true) {
      if (this.tcPatient != undefined || this.tcPatient != null) {
        return true;
      }
      else {
        return false;
      }
    } else {
      return false;
    }
  }

  isLogin() {
    return this.loginStatus;
  }

  getUserType() {
    return this.userType;
  }

  naviateTo(portal) {
    this.router.navigate([`/${portal}`]);
  }
}
