import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {

  // private API = 'http://10.125.10.72:3001/api'
  private API = 'http://localhost:3000/api';
  private headers = new Headers();

  constructor(private http: Http) {
    this.headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
  }

  getInterpreterStatus(obj) {
    return this.http
      .post(`${this.API}/interpreter/status`, obj)
      .map(res => res.json());
  }

  getQuestionInGroup(lang) {
    return this.http
      .get(`${this.API}/question/showQuestionInGroup/${lang}`)
      .map(res => res.json());
  }

  getQuestionsLanguage() {
    return this.http
      .get(`${this.API}/language`)
      .map(res => res.json());
  }

  getSettingLanguage() {
    return this.http
      .get(`${this.API}/setting/language`)
      .map(res => res.json());
  }

  getSettingLanguageIsDuplicate(langName, oldName) {
    return this.http
      .get(`${this.API}/setting/language/isDuplicate/${langName}/${oldName}`)
      .map(res => res.json());
  }

  putSettingLanguage(obj) {
    return this.http
      .put(`${this.API}/setting/language`, obj);
  }

  postSettingLanguage(obj) {
    return this.http
      .post(`${this.API}/setting/language`, obj);
  }

  getSettingGroup() {
    return this.http
      .get(`${this.API}/setting/group`)
      .map(res => res.json());
  }

  putSettingGroup(update) {
    return this.http
      .put(`${this.API}/setting/group/`, update);
  }

  getSettingGroupName(groupID) {
    return this.http
      .get(`${this.API}/setting/group/name/${groupID}`)
      .map(res => res.json());
  }

  putSettingGroupName(update) {
    return this.http
      .put(`${this.API}/setting/group/name`, update);
  }
  postSettingGroupName(update) {
    return this.http
      .post(`${this.API}/setting/group/name`, update);
  }

  postSettingGroup(add) {
    return this.http
      .post(`${this.API}/setting/group`, add);
  }

  getSettingGroupGenID() {
    return this.http
      .get(`${this.API}/setting/group/genID`)
      .map(res => res.text());
  }

  getSettingQuestionShowGroup(groupID) {
    return this.http
      .get(`${this.API}/setting/question/showGroup/${groupID}`)
      .map(res => res.json());
  }

  getSettingQuestionShow(questID) {
    return this.http
      .get(`${this.API}/setting/question/show/${questID}`)
      .map(res => res.json());
  }

  putSettingQuestionHead(obj) {
    return this.http
      .put(`${this.API}/setting/question/head`, obj);
  }

  putSettingQuestionMain(obj) {
    return this.http
      .put(`${this.API}/setting/question/main`, obj);
  }

  getSettingQuestionGenID() {
    return this.http
      .get(`${this.API}/setting/question/genID`)
      .map(res=>res.json());
  }
  
  getGroupQuestion(lang) {
    return this.http
      .get(`${this.API}/question/groupQuestion/${lang}`)
      .map(res => res.json());
  }

  getTypeOfQuestion() {
    return this.http
      .get(`${this.API}/setting/typeOfQuestion`)
      .map(res => res.json());
  }

  postTrakCarePatient(HN) {
    let postHN = 'HN=' + HN;

    return this.http
      .post(`http://10.125.10.46/API/CSI_IPD/postPatientIPD.php`, postHN, {
        headers: this.headers
      })
      .map(res => res.json());
  }

  postPicturePatient(HN) {
    let postHN = 'HN=' + HN;

    return this.http
      .post(`http://10.125.10.46/API/CSI_IPD/postPicturePatient.php`, postHN, {
        headers: this.headers
      })
      .map(res => res.json());
  }

  postEmployeeLogin(username, password) {
    const objLogin = {
      username: username,
      password: password
    };
    return this.http
      .post(`${this.API}/centerDB/login`, objLogin)
      .map(res => res.json());
  }

  putGroupOrder(order, id) {
    const objUpdate = {
      groupID: id,
      groupOrder: order
    }
    return this.http
      .put(`${this.API}/setting/group/setOrder`, objUpdate);
  }

}
