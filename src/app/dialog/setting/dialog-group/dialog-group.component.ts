import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ApiService } from '../../../service/api.service';

import { SettingGroup, SettingGroupName } from '../../../interface/setting';
@Component({
  selector: 'app-dialog-group',
  templateUrl: './dialog-group.component.html',
  styleUrls: ['./dialog-group.component.scss'],
  providers: [
    ApiService
  ]
})
export class DialogGroupComponent implements OnInit {

  state = "Add";
  group: SettingGroup = new SettingGroup;
  groupName: Array<SettingGroupName>;

  // groupID = "";
  subQry: any;
  subQrySpare: any;

  constructor(
    private dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api: ApiService
  ) {
    const checker = data;
    if (checker) {
      this.state = "Edit";
      this.group = checker;
      // this.groupID = checker.groupID;
    }

    this.subQry = this.api.getSettingGroupName(this.group.groupID).subscribe(
      data => {
        this.groupName = data;
      }
    )
  }

  ngOnInit() {
  }

  submit() {

    if (this.state == 'Add') {
      this.subQry = this.api.getSettingGroupGenID().subscribe(
        res => {
          this.group.groupID = res;
          this.subQrySpare = this.api.postSettingGroup(this.group).subscribe();

          this.groupName.forEach(e => {
            const update = {
              tranText: e.tranText,
              tranID: this.group.groupID,
              nationID: e.nationID
            }
            this.subQry = this.api.postSettingGroupName(update).subscribe();
          });

        }
      );


    }
    else if (this.state == 'Edit') {
      this.subQry = this.api.putSettingGroup(this.group).subscribe();

      this.groupName.forEach(e => {
        const update = {
          tranText: e.tranText,
          tranID: this.group.groupID,
          nationID: e.nationID
        }
        this.subQry = this.api.putSettingGroupName(update).subscribe();
      });
    }

    this.dialogRef.close(true);
  }

}
