import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ApiService } from '../../../service/api.service';

import { SettingLanguage } from '../../../interface/setting';

@Component({
  selector: 'app-dialog-language',
  templateUrl: './dialog-language.component.html',
  styleUrls: ['./dialog-language.component.scss'],
  providers: [
    ApiService
  ]
})
export class DialogLanguageComponent implements OnInit {
  languages: SettingLanguage = new SettingLanguage();
  state = "Add";

  oldName = "";
  subDuplicate:any;
  subQuery:any;

  constructor(
    private dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api: ApiService
  ) {
    if (data) {
      this.state = "Edit";
      this.languages = data;
      this.oldName = data.nationID;
    }
  }

  ngOnInit() {
  }


  submit(form) {
    this.subDuplicate = this.api.getSettingLanguageIsDuplicate(this.languages.nationID, this.oldName).subscribe(
      data => {
        if (data.flg == false) {
          if (this.state == 'Edit') {
            this.update();
          } else {
            this.add();
          }
          this.dialogRef.close(true);
        }
        else {

        }
      }
    )
  }

  update() {
    this.languages.nationOldName = this.oldName;
    this.subQuery = this.api.putSettingLanguage(this.languages).subscribe();
  }

  add() {
    this.subQuery = this.api.postSettingLanguage(this.languages).subscribe();
  }

}
