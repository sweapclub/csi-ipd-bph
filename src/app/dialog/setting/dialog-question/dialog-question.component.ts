import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ApiService } from '../../../service/api.service';

import { SettingQuestionModified, SettingQuestion } from '../../../interface/setting';
import { QuestionType } from '../../../interface/setting';

@Component({
  selector: 'app-dialog-question',
  templateUrl: './dialog-question.component.html',
  styleUrls: ['./dialog-question.component.scss'],
  providers: [
    ApiService
  ]
})
export class DialogQuestionComponent implements OnInit {
  state = 'Add';
  // groupID = "";
  subQry: any;
  subQrySpare: any;

  orderNo: number = 1;

  questionTypeID = "";

  question: Array<SettingQuestionModified>;
  questionHeader: SettingQuestion = new SettingQuestion();
  questionType: QuestionType;

  constructor(
    private dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api: ApiService
  ) {
    const checker = data;
    let questionID = "temp";
    if (checker) {
      this.orderNo = data.orderNo;
      this.questionHeader = data;
      this.state = "Edit";
      questionID = data.questionID;
    }

    this.subQry = this.api.getSettingQuestionShow(questionID).subscribe(
      data => {
        this.question = data;
      }
    )
  }

  ngOnInit() {
    this.subQry = this.api.getTypeOfQuestion().subscribe(res => {
      this.questionType = res;
    })
  }

  submit() {

    if (this.state == 'Add') {

      this.subQry = this.api.getSettingQuestionGenID().subscribe(
        res => {
          console.log(res.genID);
          this.question.forEach( e => {
            e.questionActive = this.questionHeader.questionActive;
            e.questionID = res.genID;
            console.log(e);
            // this.subQry = this.api.putSettingQuestionMain(e).subscribe();
          })
        }
      )
      // this.subQry = this.api.getSettingGroupGenID().subscribe(
      //   res => {
      //     this.group.groupID = res;
      //     this.subQrySpare = this.api.postSettingGroup(this.group).subscribe();

      //     this.groupName.forEach(e => {
      //       const update = {
      //         tranText: e.tranText,
      //         tranID: this.group.groupID,
      //         nationID: e.nationID
      //       }
      //       this.subQry = this.api.postSettingGroupName(update).subscribe();
      //     });

      //   }
      // );
    }
    else if (this.state == 'Edit') {
      // this.subQry = this.api.putSettingGroup(this.question).subscribe();

      // this.groupName.forEach(e => {
      //   const update = {
      //     tranText: e.tranText,
      //     tranID: this.group.groupID,
      //     nationID: e.nationID
      //   }
      //   this.subQry = this.api.putSettingGroupName(update).subscribe();
      // });

      const objHead = {
        isForCalculation: this.questionHeader.isForCalculation,
        orderNo: this.orderNo,
        questionID: this.questionHeader.questionID
      }
      // console.log(objHead);
      this.subQry = this.api.putSettingQuestionHead(objHead).subscribe();

      this.question.forEach( e => {
        e.questionActive = this.questionHeader.questionActive;
        this.subQry = this.api.putSettingQuestionMain(e).subscribe();
      })
    }

    this.dialogRef.close(true);
  }
}
