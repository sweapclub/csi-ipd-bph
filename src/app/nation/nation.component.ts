import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { ApiService } from '../service/api.service';
import { HelpService } from '../service/help.service';
import { Language } from '../interface/language';
@Component({
  selector: 'app-nation',
  templateUrl: './nation.component.html',
  styleUrls: ['./nation.component.scss'],
  providers: [ApiService]
})
export class NationComponent implements OnInit {

  subQuery: Subscription;
  objectLangs = [];

  constructor(
    private api: ApiService,
    private helper: HelpService
  ) {
    this.subQuery = this.api.getQuestionsLanguage().subscribe( data => {
      // this.langs = data;
      console.log(data);

      let step: number = 0;
      let obj = [];
      for (let i = 0; i < data.length; i++){
   
        obj.push(data[i]);

        if ( (i + 1) % 3 == 0) {
          this.objectLangs.push(obj);
          obj = [];
          step++;
        }
      }
      if (obj.length != 0) {
        this.objectLangs.push(obj);
      }
    })
  }

  ngOnInit() {
  }

  gotoQuestion(lang) {
    this.helper.naviateTo(`/patient/questionaire/${lang}`);
  }

}
