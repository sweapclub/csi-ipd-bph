import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuthGuardPatientService } from './auth-guard.service';

// import { AppComponent } from '../app.component';
import { LoginComponent } from '../login/login.component';
import { QuestionareComponent } from '../questionare/questionare.component';
import { NationComponent } from '../nation/nation.component';
import { MainPatientComponent } from '../main-patient/main-patient.component';
import { PatientHomeComponent } from '../patient-home/patient-home.component';

import { MainEmployeeComponent } from '../main-employee/main-employee.component';
import { SettingComponent } from '../setting/setting.component';
import { SettingLanguageComponent } from '../setting/setting-language/setting-language.component';
import { SettingGroupComponent } from '../setting/setting-group/setting-group.component';
import { SettingQuestionComponent } from '../setting/setting-question/setting-question.component';

const routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'employee', component: MainEmployeeComponent,
    children: [
      {
        path: 'setting', component: SettingComponent,
        children: [
          { path: 'language', component: SettingLanguageComponent },
          { path: 'question/:groupId', component: SettingQuestionComponent },
          { path: 'group', component: SettingGroupComponent }
        ]
      }
    ]
  },
  {
    path: 'patient', component: MainPatientComponent,
    children: [
      // , canActivate: [AuthGuardPatientService] 
      // { path: '', redirectTo: 'patient', pathMatch: 'full' },
      { path: '', component: PatientHomeComponent, canActivate: [AuthGuardPatientService] },
      { path: 'pick-nation', component: NationComponent, canActivate: [AuthGuardPatientService] },
      { path: 'questionaire/:lang', component: QuestionareComponent, canActivate: [AuthGuardPatientService] },
    ]
  },
  { path: '**', redirectTo: '/login' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: [AuthGuardPatientService]

})
export class RoutingModule { }
