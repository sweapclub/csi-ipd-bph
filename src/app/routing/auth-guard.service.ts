import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { HelpService } from '../service/help.service';

@Injectable()
export class AuthGuardPatientService implements CanActivate {
    constructor(
        private router: Router,
        private helper: HelpService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        //route.routeConfig.path
        const isAllow = this.helper.checkPermission();
        if (isAllow) {
            return true;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}

