import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../service/api.service';
import { SettingGroup } from '../../interface/setting';
import { HelpService } from '../../service/help.service';
import { DialogService } from '../../service/dialog.service';
@Component({
  selector: 'app-setting-group',
  templateUrl: './setting-group.component.html',
  styleUrls: ['./setting-group.component.scss'],
  providers:[
    DialogService
  ]
})
export class SettingGroupComponent implements OnInit {
  groups: Array<SettingGroup>;

  sub: any;
  constructor(
    private api: ApiService,
    private helper: HelpService,
    private dialog: DialogService
  ) {
    this.refresh();
  }

  ngOnInit() {
  }

  refresh() {
    this.sub = this.api.getSettingGroup().subscribe(data => {
      this.groups = data;
    });
  }

  arrowUp(index) {
    this.groups[index].groupOrder = index;
    this.groups[index - 1].groupOrder = index + 1;

    this.groups.sort(function (a, b) { return (a.groupOrder > b.groupOrder) ? 1 : ((b.groupOrder > a.groupOrder) ? -1 : 0); });
    this.updateGroupOrder(this.groups[index].groupOrder, this.groups[index].groupID);
    this.updateGroupOrder(this.groups[index - 1].groupOrder, this.groups[index - 1].groupID);
  }

  arrowDown(index) {
    this.groups[index].groupOrder = index + 2;
    this.groups[index + 1].groupOrder = index + 1;

    this.groups.sort(function (a, b) { return (a.groupOrder > b.groupOrder) ? 1 : ((b.groupOrder > a.groupOrder) ? -1 : 0); });
    this.updateGroupOrder(this.groups[index].groupOrder, this.groups[index].groupID);
    this.updateGroupOrder(this.groups[index + 1].groupOrder, this.groups[index + 1].groupID);
  }

  updateGroupOrder(id, order) {
    this.sub = this.api.putGroupOrder(id, order).subscribe();
  }

  goToQuestion(gID) {
    this.helper.naviateTo(`/employee/setting/question/${gID}`);
  }

  addGroup() {
    this.dialog.openSettingGroup().afterClosed().subscribe(
      (res) => {
        if (res) {
          this.refresh();
        }
      }
    )
  }

  editGroup(group) {
    const obj = group;
    this.dialog.openSettingGroup(obj).afterClosed().subscribe(
      res => {
        if (res) {
          this.refresh();
        }
      }
    )
  }



}
