import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../service/api.service';
import { HelpService } from '../../service/help.service';
import { SettingQuestion } from '../../interface/setting';
import { DialogService } from '../../service/dialog.service';

@Component({
  selector: 'app-setting-question',
  templateUrl: './setting-question.component.html',
  styleUrls: ['./setting-question.component.scss'],
  providers: [
    DialogService
  ]
})
export class SettingQuestionComponent implements OnInit {

  subQry: any;

  subQuestion: any;
  questions: SettingQuestion;

  groupID = "";

  constructor(
    private route: ActivatedRoute,
    private api: ApiService,
    private helper: HelpService,
    private dialog: DialogService
  ) {
  }

  ngOnInit() {
    this.subQry = this.route.params.subscribe(params => {
      // this.groupName = params['groupName'];
      this.groupID = params['groupId'];
      // this.subQuestion = this.api.getSettingQuestion(params['groupId']).subscribe(data => {
      //   this.questions = data;
      //   console.log(this.questions);
      // });
      this.refresh();

    });
  }

  refresh() {
    this.subQuestion = this.api.getSettingQuestionShowGroup(this.groupID).subscribe(data => {
      this.questions = data;
    });
  }

  addQuestion() {
    this.dialog.openSettingQuestion().afterClosed().subscribe(
      res => {
        if (res) {
          this.refresh();
        }
      }
    )
  }

  editQuestion(question) {
    const obj = question;
    this.dialog.openSettingQuestion(obj).afterClosed().subscribe(
      res => {
        if (res) {
          this.refresh();
        }
      }
    )
  }

}
