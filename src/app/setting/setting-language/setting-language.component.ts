import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../service/api.service';
import { SettingLanguage } from '../../interface/setting';
import { DialogService } from '../../service/dialog.service';

@Component({
  selector: 'app-setting-language',
  templateUrl: './setting-language.component.html',
  styleUrls: ['./setting-language.component.scss'],
  providers: [
    DialogService
  ]
})
export class SettingLanguageComponent implements OnInit {
  languages: SettingLanguage;

  sub: any;
  constructor(
    private api: ApiService,
    private dialog: DialogService
  ) {
    this.refresh();
  }

  ngOnInit() {
  }

  refresh() {
    this.sub = this.api.getSettingLanguage().subscribe(data => {
      this.languages = data;
    })
  }

  addLanguage() {
    this.dialog.openSettingLanguage().afterClosed().subscribe(
      (res) => {
        if (res) {
          this.refresh();
        }
      }
    )
  }

  editLanguage(obj) {
    const langObj = obj;
    this.dialog.openSettingLanguage(langObj).afterClosed().subscribe(
      (res) => {
        if (res) {
          this.refresh();
        }
      }
    );
  }

}
