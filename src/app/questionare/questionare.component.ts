import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../service/api.service';

import {
  QuestionInGroup,
  GroupQuestion
} from '../interface/questionaire';
import { Language } from '../interface/language';

@Component({
  selector: 'app-questionare',
  templateUrl: './questionare.component.html',
  styleUrls: ['./questionare.component.scss']
})
export class QuestionareComponent implements OnInit {

  questions: Array<QuestionInGroup>;
  subQry: any;

  prologue:boolean = true;

  showPanel = "";

  panelOpenState: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private api: ApiService
  ) { }

  ngOnInit() {
    this.subQry = this.route.params.subscribe(params => {
      this.getQuestionaire(params['lang']);
    });
  }

  getQuestionaire(lang) {
    this.subQry = this.api.getQuestionInGroup(lang).subscribe(data => {
      let tmpHeaderGroup = '';
      let tmpPlace = '';
      for (let i = 0; i < data.length; i++) {
        if (tmpHeaderGroup == data[i].groupText) {
          data[i].groupText = ""
        } else {
          tmpHeaderGroup = data[i].groupText;
        }

        if ([data[i].usePlace == '1']) {
          if (tmpPlace == data[i].placeName) {
            data[i].placeName = ""
          } else {
            tmpPlace = data[i].placeName;
          }
        }
      }
      this.questions = data;
    })
  }

  keepScore(obj, index) {
    // console.log(obj.value);
    if (this.questions[index].typeName == 'Choice') {
      switch (obj.value) {
        case this.questions[index].score1:
          this.questions[index].answer = 1;
          this.questions[index].score = obj.value;
          break;
        case this.questions[index].score2:
          this.questions[index].answer = 2;
          this.questions[index].score = obj.value;
          break;
        case this.questions[index].score3:
          this.questions[index].answer = 3;
          this.questions[index].score = obj.value;
          break;
        case this.questions[index].score4:
          this.questions[index].answer = 4;
          this.questions[index].score = obj.value;
          break;
        case this.questions[index].score5:
          this.questions[index].answer = 5;
          this.questions[index].score = obj.value;
          break;
        default:
          this.questions[index].answer = null;
          this.questions[index].score = null;
          break;
      }
    } else if (this.questions[index].typeName == 'Score') {
      this.questions[index].answer = obj.value;
      this.questions[index].score = obj.value;
    } else if (this.questions[index].typeName == 'Suggestion') {
      this.questions[index].answer = obj.text;
      this.questions[index].score = obj.text;
    }
    // if this.questions[index].
    // console.log(index);
    console.log(this.questions[index]);
    // this.questions[index].answer = 
  }

  setShowPanel(panelName: string) {
    this.showPanel = panelName;
  }

  placeChecked(index, groupID, placeID) {
    for (let i = index; i <= this.questions.length; i++) {
      if (this.questions[i].groupID != groupID || this.questions[i].placeID != placeID){
        break;
      }
      this.questions[i].placeSelected = !this.questions[i].placeSelected;
    }
  }
}
