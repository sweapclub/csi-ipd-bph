import { Component, OnInit } from '@angular/core';

import { ApiService } from '../service/api.service';
import { HelpService } from '../service/help.service';

import {
  TrakCarePatient,
  PicturePatient
} from '../interface/patient';

@Component({
  selector: 'app-patient-home',
  templateUrl: './patient-home.component.html',
  styleUrls: ['./patient-home.component.scss']
})
export class PatientHomeComponent implements OnInit {

  patient: TrakCarePatient;
  picture: PicturePatient;

  sub:any;

  constructor(
    private api: ApiService,
    private helper: HelpService
  ) {
    this.patient = this.helper.tcPatient[0];
    console.log(this.patient);

    this.sub = this.api.postPicturePatient(this.patient.patHN).subscribe(data => {
      console.log(data);
      if (data != null ){
        if (data[0].LinkPhoto != "X"){
          this.picture = data[0];
        }
      }
    })
  }


  ngOnInit() {
  }

  correctHN() {
    this.helper.naviateTo('/patient/pick-nation');
  }

  notCorrectHN() {
    this.helper.naviateTo('/login');
  }
}
