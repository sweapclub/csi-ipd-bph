import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatIconModule,
  MatCardModule,
  // MatListModule,
  MatButtonModule,
  // MatDatepickerModule,
  // MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatDialogModule,
  MatCheckboxModule,
  // MatSlideToggleModule,
  MatMenuModule,
  // MatTooltipModule
  MatToolbarModule,
  MatSnackBarModule,
  MatDividerModule,
  MatTabsModule
} from '@angular/material';

import { RoutingModule } from './routing/routing.module';
import { HelpService } from './service/help.service';

import { AppComponent } from './app.component';
import { QuestionareComponent } from './questionare/questionare.component';
import { NationComponent } from './nation/nation.component';
import { MainPatientComponent } from './main-patient/main-patient.component';
import { LoginComponent } from './login/login.component';
import { PatientHomeComponent } from './patient-home/patient-home.component';
import { MainEmployeeComponent } from './main-employee/main-employee.component';
import { SettingComponent } from './setting/setting.component';
import { SettingLanguageComponent } from './setting/setting-language/setting-language.component';
import { SettingGroupComponent } from './setting/setting-group/setting-group.component';
import { SettingQuestionComponent } from './setting/setting-question/setting-question.component';
import { DialogLanguageComponent } from './dialog/setting/dialog-language/dialog-language.component';
import { DialogGroupComponent } from './dialog/setting/dialog-group/dialog-group.component';
import { DialogQuestionComponent } from './dialog/setting/dialog-question/dialog-question.component';

@NgModule({
  declarations: [
    AppComponent,
    QuestionareComponent,
    NationComponent,
    MainPatientComponent,
    LoginComponent,
    PatientHomeComponent,
    MainEmployeeComponent,
    SettingComponent,
    SettingLanguageComponent,
    SettingGroupComponent,
    SettingQuestionComponent,
    DialogLanguageComponent,
    DialogGroupComponent,
    DialogQuestionComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutingModule,
    FormsModule,
    HttpModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatIconModule,
    MatMenuModule,
    MatDividerModule,
    MatDialogModule,
    MatTabsModule,
    MatSelectModule
  ],
  providers: [HelpService],
  bootstrap: [AppComponent],
  entryComponents: [
    DialogLanguageComponent,
    DialogGroupComponent,
    DialogQuestionComponent
  ],
})
export class AppModule { }
