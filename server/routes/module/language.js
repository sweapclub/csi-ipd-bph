const mysql = require('../mysql');
const express = require('express');
const router = express.Router();

router.get('/', function (req, res) {
  const obj = req.params;
  mysql.query(`select nationID,nationName,nationShow
  from Nation
  where nationactive = '1'
  order by nationID
  `, [obj.nation, obj.groupID],
    function (err, result) {
      if (err) throw err;
      res.send(result);
    });
})

router.get('/manage', function (req, res) {
  const obj = req.params;
  mysql.query(`select nationID,nationName,nationShow,nationActive
  from Nation
  order by nationID
  `, [obj.nation, obj.groupID],
    function (err, result) {
      if (err) throw err;
      res.send(result);
    });
})

module.exports = router;
