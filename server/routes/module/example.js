const mysql = require('../mysql');
const express = require('express');
const router = express.Router();

router.get('/assignWorkCurrent', function (req, res) {
  mysql.query(`SELECT cow.assignDate, cow.admitDate, cow.patientHN, patientName, patientAge, nationality, admitWard, admitRoom, fullName
  FROM tbcurrent_onward_8am AS cow
  LEFT JOIN tbassignwork_interpreter AS asInter ON asInter.assignDate = cow.assignDate
  AND asinter.patientHN = cow.patientHN
  LEFT JOIN tbuserlogin AS usrInter ON usrInter.username = asinter.interpreterID
  WHERE cow.assignDate = DATE( NOW( ) ) `,
    function (err, result) {
      if (err) throw err;
      res.send(result);
    });
})

router.post('/assignInterpreter', (req, res) => {
  if (!req.body) return res.sendStatus(400)
  const obj = req.body;
  mysql.query(`
  SELECT CONCAT( SUBSTRING( REPLACE( DATE( NOW( ) ) ,  '-',  '' ) , 1, 6 ) , LPAD( CONVERT( SUBSTRING( rw_id, 7, 4 ) , signed ) +1, 4,  '0' ) )
  AS RID
FROM tbrequest_work
WHERE SUBSTRING( rw_id, 1, 6 ) = SUBSTRING( REPLACE( DATE( NOW( ) ) ,  '-',  '' ) , 1, 6 )
ORDER BY RW_id DESC
LIMIT 1;
  `, function (err, result) {
        res.send(insertWork(obj));
    });
})

function insertWork(obj) {
  // mysql.query(`
  //   insert into tbassignwork_interpreter(assignDate, patientHN, interpreterID)
  //   value ('${obj.admitDate}','${obj.patientHN}','${obj.interpreterID}')
  // `, (err, result) => {
  //     if (err) throw err;
  //     //console.log('insert');
  //     return (result);
  //   });
}

module.exports = router;
