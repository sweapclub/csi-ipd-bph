const mysql = require('../mysql');
const express = require('express');
const connection = require('../msSql');
var Request = require('tedious').Request;
const router = express.Router();

function doConn() {
  connection.on('connect', function (err) {
    if (err) {
      console.log(err);
    } else {
      executeStatement();
    }
  });
}

function closeConn() {
  connection.close();
}

//center DB

router.post('/employee/login', (req, res) => {
  var jsonArray = [];
  var rowObject = {};

  if (!req.body) return res.sendStatus(400)
  const obj = req.body;

  doConn();

  request = new Request(`
  SELECT st_id as staffID, st_pwd as staffPassword,
      st_pname_th + st_fname_th + ' ' + st_lname_th as staffName,
      dept_id as depID, dept_name_th as depName
  FROM staff 
  WHERE st_usern = '${username}' AND st_pwd = '${password}' 
      AND st_active = '1'
  `, (err, rowCounts) => {
      if (err) throw err;
    });

  request.on('row', function (columns) {
    rowObject = {};
    columns.forEach(function (column) {
      var tempColName = column.metadata.colName;
      var tempColData = column.value;
      rowObject[tempColName] = tempColData;
    })
    jsonArray.push(rowObject);
  });

  request.on('doneProc', function (rowCount, more) {
    res.send(jsonArray);
  });

  connection.execSql(request, function () {
    closeConn();
  });
});


//MySql
router.post('/patient/login', (req, res) => {
  if (!req.body) return res.sendStatus(400)
  const obj = req.body;
  mysql.query(`
    SELECT login. UserName , FullName , OfficePhone , MobilePhone , UserType 
    , cast(pageMyJob as char(1)) as pageMyJob 
    , cast(pageMyRequest  as char(1)) as pageMyRequest
    , cast(pageNewJob  as char(1)) as pageNewJob
    , cast(pageJobList  as char(1)) as pageJobList
    , cast(pageInterpreterStatus  as char(1)) as pageInterpreterStatus
    , cast(onlyArabic  as char(1)) as onlyArabic
    , cast(assignRequest  as char(1)) as assignRequest
    , cast(dashBoard  as char(1)) as dashBoard
    , cast(manageUser  as char(1)) as manageUser
    , cast(manageInterpreter  as char(1)) as manageInterpreter
    , cast(manageLanguage  as char(1)) as manageLanguage
    , cast(manageCancelReason as char(1)) as manageCancelReason
    , Department
    ,	cast(manageCheckIn  as char(1)) as manageCheckIn
    , cast(reportRaw as char(1)) as reportRaw
    , cast(reportRatio as char(1)) as reportRatio
    , cast(pageRegistWaitingTime as char(1)) as pageRegistWaitingTime
    FROM  tbuserlogin as login left join tbpermission_page as pms on login.UserName = pms.UserName
    WHERE login.UserName = '${obj.userName}' and Password = '${obj.password}'`
   , function (err, result) {
        res.send(result);
    });
})

module.exports = router;
