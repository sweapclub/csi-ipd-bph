const connection = require('../msSql');
const express = require('express');
const router = express.Router();
var Request = require('tedious').Request;

function doConn() {
  connection.on('connect', function (err) {
    if (err) {
      console.log(err);
    } else {
      executeStatement();
    }
  });
}

function closeConn() {
  connection.close();
}


router.post('/login', (req, res) => {
    var jsonArray = [];
    var rowObject = {};
  
    if (!req.body) return res.sendStatus(400)
    const obj = req.body;

    doConn();
  
    request = new Request(`
    SELECT st_id as staffID, st_pwd as staffPassword,
        st_pname_th + st_fname_th + ' ' + st_lname_th as staffName,
        department.dept_id as depID, dept_name_th as depName
    FROM [CENTER_DB].[dbo].[STAFF] as staff 
    inner join [CENTER_DB].[dbo].[DEPARTMENT] as department on staff.dept_id = department.dept_id
    WHERE st_usern = '${obj.username}' AND st_pwd = '${obj.password}' 
        AND st_active = '1'
    `, (err, rowCounts) => {
        if (err) throw err;
      });
  
    request.on('row', function (columns) {
      rowObject = {};
      columns.forEach(function (column) {
        var tempColName = column.metadata.colName;
        var tempColData = column.value;
        rowObject[tempColName] = tempColData;
      })
      jsonArray.push(rowObject);
    });
  
    request.on('doneProc', function (rowCount, more) {
      res.send(jsonArray);
    });
  
    connection.execSql(request, function () {
      closeConn();
    });
  });
  
  module.exports = router;
  