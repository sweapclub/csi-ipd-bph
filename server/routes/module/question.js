const mysql = require('../mysql');
const express = require('express');
const router = express.Router();


router.get('/groupQuestion/:lang', function(req,res){
  const lang = req.params.lang;
  mysql.query(`
    select tranID as groupID ,tranText as groupName
    from GroupMaster as GM
    left join GroupTranslate as GT on GM.groupID = GT.tranID
    where groupactive = 1 and nationID = '${lang}'
    order by groupOrder
  `, (err,result) => {
    if (err) throw err;
    res.send(result);
  }) 
});

router.get('/showQuestionInGroup/:nation', function (req, res) {
  const obj = req.params;
  mysql.query(
    
  //   `select 
  //   GM.groupID,Q.questionID,
  //   Q.questionText,Q.nationID,
  //   Q.choice1,Q.score1,
  //   Q.choice2,Q.score2,
  //   Q.choice3,Q.score3,
  //   Q.choice4,Q.score4,
  //   Q.choice5,Q.score5,
  //   Q.useTextboxFlg,Q.useTextboxAt
  //   from GroupMatch as GM
  //   inner join Question as Q on GM.questionID = Q.questionID
  //   inner join GroupMaster as GM on GM.
  //   where nationID = ? and Q.questionActive = '1'
  //     and GM.groupID = ?
  //   order by GM.groupID, GM.orderNo
  // `
    // `
    // select 
    //   GM.groupID,Q.questionID,
    //   Q.nationID,
    //   GT.tranText as groupText,
		// 	GM.orderNo as questionNo,
    //   Q.questionText,
    //   Q.choice1,Q.score1,
    //   Q.choice2,Q.score2,
    //   Q.choice3,Q.score3,
    //   Q.choice4,Q.score4,
    //   Q.choice5,Q.score5,
    //   Q.useTextboxFlg,Q.useTextboxAt,
		// 	QT.typeID,QT.typeName
    //   from GroupMatch as GM
    //   inner join Question as Q on GM.questionID = Q.questionID
    //   inner join GroupMaster as G on G.groupID = GM.groupID
		// 	inner join QuestionType as QT on Q.questionTypeID = QT.typeID 
    //   left join GroupTranslate as GT on GM.groupID = GT.tranID and Q.nationID = GT.nationID 
    //   where Q.nationID = ? and Q.questionActive = '1'and G.groupActive = '1'
    //   order by G.groupOrder,GM.orderNo
    // `

    `
    select 
    case G.usePlace
    when 0 THEN Q.questionID
    when 1 THEN	CONCAT(Q.questionID, "-" , P.placeID)
    end as domID,
    GM.groupID,
    Q.questionID,Q.nationID,
    GT.tranText as groupText,
    GM.orderNo as questionNo,
    Q.questionText,
    Q.choice1,Q.score1,
    Q.choice2,Q.score2,
    Q.choice3,Q.score3,
    Q.choice4,Q.score4,
    Q.choice5,Q.score5,
    Q.useTextboxFlg,Q.useTextboxAt,
    QT.typeID,QT.typeName,
    P.placeName,P.placeID,
    G.usePlace
    from GroupMatch as GM
    inner join Question as Q on GM.questionID = Q.questionID
    inner join GroupMaster as G on G.groupID = GM.groupID
    inner join QuestionType as QT on Q.questionTypeID = QT.typeID 
    left join GroupTranslate as GT on GM.groupID = GT.tranID and Q.nationID = GT.nationID 
    left join Place as P on G.usePlace = 1 and P.nationID = Q.nationID
    where Q.nationID = ? and Q.questionActive = '1'and G.groupActive = '1'
    order by G.groupOrder, 
      case G.usePlace when 0 then GM.orderNo  when 1 then P.placeID end,
      case G.usePlace when 1 then GM.orderNo end
    `
  , [obj.nation],
    function (err, result) {
      if (err) throw err;
      res.send(result);
    });
})

// router.post('/assignInterpreter', (req, res) => {
//   if (!req.body) return res.sendStatus(400)
//   const obj = req.body;
//   mysql.query(`
//   SELECT CONCAT( SUBSTRING( REPLACE( DATE( NOW( ) ) ,  '-',  '' ) , 1, 6 ) , LPAD( CONVERT( SUBSTRING( rw_id, 7, 4 ) , signed ) +1, 4,  '0' ) )
//   AS RID
// FROM tbrequest_work
// WHERE SUBSTRING( rw_id, 1, 6 ) = SUBSTRING( REPLACE( DATE( NOW( ) ) ,  '-',  '' ) , 1, 6 )
// ORDER BY RW_id DESC
// LIMIT 1;
//   `, function (err, result) {
//       res.send(insertWork(obj));
//     });
// })

// function insertWork(obj) {
//   // mysql.query(`
//   //   insert into tbassignwork_interpreter(assignDate, patientHN, interpreterID)
//   //   value ('${obj.admitDate}','${obj.patientHN}','${obj.interpreterID}')
//   // `, (err, result) => {
//   //     if (err) throw err;
//   //     //console.log('insert');
//   //     return (result);
//   //   });
// }

module.exports = router;
