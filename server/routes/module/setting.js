const mysql = require('../mysql');
const express = require('express');
const router = express.Router();

router.get('/language', function (req, res) {
  const obj = req.params;
  mysql.query(`select nationID,nationName,nationShow,nationActive
  from Nation
  order by nationID
  `, [obj.nation, obj.groupID],
    function (err, result) {
      if (err) throw err;
      res.send(result);
    });
})

router.get('/language/isDuplicate/:langName/:oldName?', function (req, res) {
  const langName = req.params.langName;
  const oldName = req.params.oldName;
  mysql.query(`
    select *
    from (select * from Nation where nationID <> ? ) as tmp
    where nationID = ? 
  `, [oldName, langName], (err, result) => {
      if (err) throw err;
      if (result.length == 0) {
        res.send({ flg: false });
      } else {
        res.send({ flg: true });
      }
    });
})

router.put('/language', (req, res) => {
  if (!req.body) return res.sendStatus(400)
  const obj = req.body;
  mysql.query(`
  UPDATE Nation
  SET nationID = ?,
    nationName = ?,
    nationShow = ?,
    nationActive = ?
  WHERE nationID = ?
  `, [
      obj.nationID,
      obj.nationName,
      obj.nationShow,
      obj.nationActive,
      obj.nationOldName
    ], (err, result) => {
      if (err) throw err;
      res.send();
    })
})

router.post('/language', (req, res) => {
  if (!req.body) return res.sendStatus(400)
  const obj = req.body;
  mysql.query(`
  insert into Nation
  values (?,?,?,?)
  `, [obj.nationID, obj.nationName, obj.nationShow, obj.nationActive], (err, result) => {
      if (err) throw err;
      res.send();
    })
})

// End Language

router.route('/group')
  .get(function (req, res) {
    mysql.query(`select groupID,groupActive,usePlace,tranText as groupName,groupOrder
    from GroupMaster as GM
    left join GroupTranslate as GT on GM.groupID = GT.tranID and GT.nationID = 'Thai'
    order by groupOrder
  `,
      function (err, result) {
        if (err) throw err;
        res.send(result);
      });
  })
  .put(function (req, res) {
    if (!req.body) return res.sendStatus(400)
    const obj = req.body;
    mysql.query(`
    update GroupMaster
    set groupActive = ?,
    usePlace = ?
    where groupID = ?
    `, [obj.groupActive, obj.usePlace, obj.groupID], (err, result) => {
        if (err) throw err;
        res.send();
      })
  })
  .post(function (req, res) {
    if (!req.body) return res.sendStatus(400);
    const obj = req.body;
    mysql.query(`
    insert into GroupMaster(groupID,groupActive,groupOrder,usePlace)
    select ?,?,count(*) + 1,? from GroupMaster
    `, [obj.groupID, obj.groupActive, obj.usePlace], (err, result) => {
        if (err) throw err;
        res.send();
      })
  })

router.put('/group/name', function (req, res) {
  if (!req.body) return res.sendStatus(400)
  const obj = req.body;
  mysql.query(`
  update GroupTranslate
  set tranText = ?
  where tranID = ? and nationID = ?
  `, [obj.tranText, obj.tranID, obj.nationID], (err, result) => {
      if (err) throw err;
      res.send();
    })
});

router.get('/group/genID', function (req, res) {
  mysql.query(`
  select
  case count(*)
  when 0
  then 'G01'
  else 
  (
    select concat(substring(groupID,1,1), LPAD(cast(substring(groupID,2,2) as UNSIGNED) + 1,2,'0')  ) as a
    from GroupMaster
    order by groupID desc limit 0,1
  )
  end as groupID
  from GroupMaster
  order by groupID desc limit 0,1;
  `, (err, result) => {
      if (err) throw err;
      res.send(result[0].groupID);
    })
})

router.put('/group/setOrder', function (req, res) {
  if (!req.body) return res.sendStatus(400)
  const obj = req.body;
  mysql.query(`update GroupMaster 
  SET groupOrder = ?
  where groupID = ?
`, [obj.groupOrder, obj.groupID],
    function (err, result) {
      if (err) throw err;
      res.send();
    });
})

router.get('/group/name/:groupID?', function (req, res) {
  const groupID = req.params.groupID;
  mysql.query(`
  select N.nationID, N.nationName, ifnull(GT.tranText,'') as tranText
  from Nation as N
  left join 
  ( select *
   from GroupTranslate 
  where GroupTranslate.tranID = ?
  )
  as GT on N.nationID = GT.nationID
  order by N.nationName
  `, [groupID], (err, result) => {
      if (err) throw err;
      res.send(result);
    })
})

router.post('/group/name/', (req, res) => {
  if (!req.body) return res.sendStatus(400);
  const obj = req.body;

  mysql.query(`
  insert into GroupTranslate (tranID,nationID,tranText)
  values (?,?,?)
  `, [obj.tranID, obj.nationID, obj.tranText], (err, result) => {
      if (err) throw err;
      res.send();
    })
})

// end Group

router.get('/question/showGroup/:groupID', function (req, res) {
  const groupID = req.params.groupID;
  mysql.query(`
    select GM.orderNo, Q.questionID,Q.questionTypeID, QY.typeName as questionTypeName,Q.questionText,Q.questionActive,GM.isForCalculation
    from GroupMatch as GM
    left join Question as Q on GM.questionID = Q.questionID
    left join QuestionType as QY on Q.questionTypeID = QY.typeID
    where Q.nationID = 'Thai' and GM.groupID = ?
  `, [groupID], (err, result) => {
      if (err) throw err;
      res.send(result);
    });
})

router.get('/question/show/:questID', (req, res) => {
  const questID = req.params.questID;
  mysql.query(`
  select N.nationID, N.nationName,  Q.questionID,
  Q.questionTypeID,
  Q.questionText,
  Q.choice1,
  Q.score1,
  Q.choice2,
  Q.score2,
  Q.choice3,
  Q.score3,
  Q.choice4,
  Q.score4,
  Q.choice5,
  Q.score5,
  Q.useTextboxFlg,
  Q.useTextboxAt,
  Q.questionActive
  from Nation as  N left join (select * from Question where questionID = ?) as Q on N.nationID = Q.nationID
  order by nationName
  `, [questID], (err, result) => {
      if (err) throw err;
      res.send(result);
    });
})

router.put('/question/head', (req, res) => {
  if (!req.body) return res.sendStatus(400);
  const obj = req.body;
  mysql.query(`
  update GroupMatch
  SET isForCalculation = ?,
      orderNo = ?
  where questionID = ?
  `, [obj.isForCalculation, obj.orderNo, obj.questionID], (err, result) => {
      if (err) throw err;
      res.send();
    })
})

router.put('/question/main', (req, res) => {
  if (!req.body) return res.sendStatus(400);
  const obj = req.body;
  mysql.query(`
  update Question
  set questionTypeID = ?,
    questionText = ?,
    nationID = ?,
    choice1 = ?,
    score1 = ?,
    choice2 = ?,
    score2 = ?,
    choice3 = ?,
    score3 = ?,
    choice4 = ?,
    score4 = ?,
    choice5 = ?,
    score5 = ?,
    useTextboxFlg = ?,
    useTextboxAt = ?,
    questionActive = ?
  where questionID = ? and nationID = ?
  `, [
      obj.questionTypeID,
      obj.questionText,
      obj.nationID,
      obj.choice1,
      obj.score1,
      obj.choice2,
      obj.score2,
      obj.choice3,
      obj.score3,
      obj.choice4,
      obj.score4,
      obj.choice5,
      obj.score5,
      obj.useTextboxFlg,
      obj.useTextboxAt,
      obj.questionActive,
      obj.questionID,
      obj.nationID
    ], (err, result) => {
      if (err) throw err;
      res.send();
    })
})

router.get('/question/genID', (req, res) => {
  mysql.query(`
  select
  case count(*)
  when 0
  then 'Q001'
  else 
  (
    select concat(substring(questionID,1,1), LPAD(cast(substring(questionID,2,3) as UNSIGNED) + 1,3,'0')  ) as a
    from Question
    order by questionID desc limit 0,1
  )
  end as questionID
  from Question
  order by questionID desc limit 0,1;

  `, (err, result) => {
      if (err) throw err;
      res.send({genID:result[0].questionID});
    });
})

router.get('/typeOfQuestion/', (req, res) => {
  mysql.query(`
  select typeID,typeName,typeDescription 
  from QuestionType
  `, (err, result) => {
      if (err) throw err;
      res.send(result);
    });
})


module.exports = router;
